Endpointas: /webhooks/{supplier}/subscription



?? Kokius įrankius/kokiame lygmenyje turi būti įgyvendintas cancelinimas supportui? 

1. Webservice Aplinkos paruošimas (0.5-1.5h)

   1) Paruošti projekto griaučius
   
	- composer, docker, symfony, mysql 
	
	+ (bundle'ai) 
	
   2) Sudėti struktūrą
   
	- Controller
	
		2) SubscriptionController
		
	- DataNormalizer
	
		1) ****DataNormalizer - galima atskirti duomenų normalizavimą į atskirą metodą
		
	- Entity:
	
		1) Order
		
		2) Subscription 
		
		3) Transaction
		
        4) Receipt
        
	- Services
	
		1) ****SubscriptionManager (Konkretaus providerio Manageris raw requestui apdoroti)
		
		2) SubscriptionManager (bendra, nuo providerių nepriklausomos logikos pradžia)
 		  
 		  * addNewSubscription
		  
		  * removeSubscription
		  
		  * renewSubscription
		  
		  * addNewTransaction	
		  
	- Interface
	
		1)SubscriptionInterface

	- Command 
	
		1) AutoRevocationCommand (komanda, kuri paleidžiama crono paslaugos nutraukimui)
	
		
2. Testų paruošimas pagrindiniems use case'ams

3. Į projekto struktūrą sudėti Apple webhook integraciją (0.5-1h)

   1) Aprašyti Apple webhook elementus projekto struktūroje (controller/request/response/requesto "apdorojimo" servisas, dataNormalizeris)

4. Initial subscription ir cancel subpscription implementavimas + unit testai (2.5-4h)
  
   * Objektu aprasymas  (0.5-1h)
   
   1) addNewSubscription servisas 
   
   2) revokeSubscription servisas/
   
   3) AutoRevocationCommand 

(1-1.5h)

   4) Support cancel subscription controlleris (0.5-1h)
   
   5) Unit testai (1-1.5h)

	
5. Reniewed subscription ir Unsuccessful subscription implementavimas + unit testai
   
   1) RreniewSubscription
   
   2) Unsuccessful reniew actionas

6. Tobulinimas: 
	
	* supporto notifikavimas, nesėkmingu reniewal atveju




Objektai:


Order - turi informacija apie subscriptiona, apie vartotoja, apie ivykdytus apmokėjimus. Tai informacija apie viena kartą atliktą užsakymą

Order
  - contactInformation
  
  - Subscription
  
  - receipts
  
  - status 
  
  - orderCreatedDate
  
  - ...  

Subscription - informacija apie produkta bei galiojimo laika

Subscription 

  - productId
  
  - isAutoRenievable
  
  - isTrialPeriod 
  
  - expirationDate
  
  - expirationIntent
  
  - activationDate
  
  - ...         

Orderis turi keleta transactionu (uzsakymas, paslaugos atnaujinimas - n kartu, nutraukimas...)

Transaction

  - status
  
  - receiptInfo
  
  - transactionType 
  
  - cancelationReason (jei tipas - nutraukimas)
  
  - ...
  
Receipt (info, kurios reikia apskaitai?)

  - quantity
  
  - purchaseDate
   
  - productId
  
  - ...


Tam, kad pridėti naują providerį, reikėtų pridėti mappinimo reikšmes į SubsriptionInterface ir sukurti to providerio Managerį, kuriame būtų apdorojama raw requesto informacija ir transformuojami duomenys į independent daliai tinkamą struktūrą.


### Ką dar reikėtų padaryti? ###

* iki galo reikėtų įgyvendinti apple supplierio duomenų apdorojimą;

* Aprašyti Subscription metodų logiką;

* Aprašyti objektų struktūrą;

* Esant poreikiui būtų galima duomenų 'normalizinimą' perkelti į atskirą failą;

* Įgyvendinti autentifikaciją.
