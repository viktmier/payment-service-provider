<?php

namespace App\Interfaces;

use App\Entity\Subscription;
use App\Service\AppleWebDataManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * App\Interfaces\SubscriptionInterface
 */
interface SubscriptionInterface
{
    const SUPPLIER_APPLE = 'apple';
    const SUPPLIER_BRAINTREE = 'braintree';
    const SET_OF_SUPPLIERS = [
        self::SUPPLIER_APPLE     => AppleWebDataManager::class,
        self::SUPPLIER_BRAINTREE => 'BraintreeDataNormalizer'
    ];

    /**
     * @param Request $request
     */
    public function processRawRequest(Request $request);

    /**
     * @param Request $request
     *
     * @return Subscription
     */
    public function prepareDataForSubscription(Request $request): Subscription;
}
