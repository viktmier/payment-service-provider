<?php

namespace App\Controller;

use App\Interfaces\SubscriptionInterface;
use App\Service\SubscriptionManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * App\Controller\SubscriptionController
 */
class SubscriptionController
{
    /**
     * @var SubscriptionManager
     */
    protected $subscriptionManager;

    /**
     * SubscriptionController constructor.
     *
     * @param SubscriptionManager $subscriptionManager
     */
    public function __construct(SubscriptionManager $subscriptionManager)
    {
        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * @Route("/webhooks/{supplier}/subscription",  name="manage_subscription", methods={"POST"})
     *
     * @param Request $request
     * @param string  $supplier
     *
     * @return Response
     */
    public function manageSubscription(Request $request, string $supplier): Response
    {
        if (!isset(SubscriptionInterface::SET_OF_SUPPLIERS[$supplier])) {
            return new Response('Unknown supplier', 400);
        }

        try {
            $manager = SubscriptionInterface::SET_OF_SUPPLIERS[$supplier];
            (new $manager($this->subscriptionManager))->processRawRequest($request);
        } catch (\Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }

        return new Response('success', 200);
    }
}
