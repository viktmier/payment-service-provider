<?php

namespace App\Service;

use App\Entity\Subscription;

/**
 * App\Service\SubscriptionManager
 */
class SubscriptionManager
{
    public function addNewSubscription(Subscription $subscriptionData)
    {
        //TODO: Implement logic for adding new subscription
    }

    public function removeSubscription(Subscription $subscriptionData)
    {
        //TODO: Implement logic for removing subscription
    }


    public function renewSubscription(Subscription $subscriptionData)
    {
        //TODO: Implement logic for renewig subscription
    }
}
