<?php

namespace App\Service;

use App\Entity\Subscription;
use App\Interfaces\SubscriptionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * App\Service\AppleWebDataNormalizer
 */
class AppleWebDataManager implements SubscriptionInterface
{
    const ACTION_CANCEL = 'CANCEL';
    const ACTION_INITIAL_BUY = 'INITIAL_BUY';
    const ACTION_DID_FAIL_TO_RENEW = 'DID_FAIL_TO_RENEW';
    const ACTION_RENEWAL = 'RENEWAL';

    /**
     * @var SubscriptionManager
     */
    protected $subscriptionManager;

    /**
     * AppleWebDataManager constructor.
     *
     * @param SubscriptionManager $subscriptionManager
     */
    public function __construct(SubscriptionManager $subscriptionManager)
    {
        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     */
    public function processRawRequest(Request $request): void
    {
        $data = $this->transformJsonBody($request);
        $normalizedData =$this->prepareDataForSubscription($data);
            switch ($data->get('notification_type')) {
                case self::ACTION_CANCEL || self::ACTION_DID_FAIL_TO_RENEW:
                    $this->subscriptionManager->removeSubscription($normalizedData);
                    break;
                case self::ACTION_INITIAL_BUY:
                    $this->subscriptionManager->addNewSubscription($normalizedData);
                    break;
                case self::ACTION_RENEWAL:
                    $this->subscriptionManager->renewSubscription($normalizedData);
                    break;
                default:
                    throw new \Exception('Unknown notification_type');
            }
    }

    /**
     * @param Request $request
     *
     * @return Subscription
     */
    public function prepareDataForSubscription(Request $request): Subscription
    {
        return new Subscription(); // TODO: implement
    }

    /**
     * @param Request $request
     *
     * @return Request
     */
    protected function transformJsonBody(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }
}
